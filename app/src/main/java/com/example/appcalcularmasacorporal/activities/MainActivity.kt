package com.example.appcalcularmasacorporal.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.example.appcalcularmasacorporal.R
import java.util.*
import android.text.TextUtils
import android.widget.Toast
import kotlin.math.round


class MainActivity : AppCompatActivity() {

    private lateinit var imageResultado: ImageView
    private lateinit var txtResultado: TextView
    private lateinit var txtResultadoPeso: TextView
    private lateinit var txtAltura: TextView
    private lateinit var txtPeso: TextView
    private lateinit var btnCalcular: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        imageResultado = findViewById(R.id.image_resultado)
        txtResultado =  findViewById(R.id.txt_resultado)
        txtResultadoPeso  = findViewById(R.id.txt_resultado_peso)
        txtAltura = findViewById(R.id.txt_altura)
        txtPeso = findViewById(R.id.txt_peso)
        btnCalcular = findViewById(R.id.btn_calcular)
        imageResultado.setImageResource(R.drawable.peso_ideal)

        btnCalcular.setOnClickListener {

            var digitsOnly = TextUtils.isDigitsOnly(txtPeso.getText())
            if(!digitsOnly)
                Toast.makeText(this, getResources().getString(R.string.enter_text_label_heigth),Toast.LENGTH_LONG).show()

            digitsOnly = TextUtils.isDigitsOnly(txtAltura.getText())
            if(!digitsOnly)
                Toast.makeText(this, getResources().getString(R.string.enter_text_label_heigth),Toast.LENGTH_LONG).show()


            val boolPeso = txtPeso.text.toString().toIntOrNull()
            val isInteger = boolPeso != null

            val boolAltura = txtAltura.text.toString().toDoubleOrNull()
            val isDouble = boolAltura != null

            if(isInteger && isDouble){

                val istr1 = txtPeso.text.toString()
                val pesoInt: Int = istr1.toInt()

                val dstr1 = txtAltura.text.toString()
                val doubleAltura: Double = dstr1.toDouble()
                //
                val imc = calcular(pesoInt, doubleAltura)
                txtResultado.text = "%.2f".format(imc)
                txtResultadoPeso.text = obtenerCondicion(imc)

            }else{
                Toast.makeText(this, getResources().getString(R.string.enter_label_weigth_heigth),Toast.LENGTH_LONG).show()
            }
        }

    }

    //Ejemplo: Peso = 68 kg, Estatura = 165 cm (1.65 m) Cálculo: 68 ÷ (1.65)2 = 24.98
    fun calcular(peso:Int, altura:Double):Double{
        var imc = peso / (altura * altura)
        return  imc
    }

    fun obtenerCondicion(imc: Double):String{

        if(imc <= 17)
        {
            imageResultado.setImageResource(R.drawable.bajo_peso)
            return getResources().getString(R.string.low_weigth)

        }
        else if(imc <= 18.5)
        {
            imageResultado.setImageResource(R.drawable.bajo_peso)
            return getResources().getString(R.string.low_weigth)
        }
        else if(imc <= 24.9)
        {
            imageResultado.setImageResource(R.drawable.peso_ideal)
            return getResources().getString(R.string.ideal_weigth)
        }
        else if(imc <= 29.9)
        {
            imageResultado.setImageResource(R.drawable.obesidad)
            return getResources().getString(R.string.obesity)
        }
        else if(imc <= 34.9)
        {
            imageResultado.setImageResource(R.drawable.obesidad_extrema)
            return getResources().getString(R.string.obesity_grade_1)
        }
        else if(imc <= 39.9)
        {
            imageResultado.setImageResource(R.drawable.obesidad_extrema)
            return getResources().getString(R.string.obesity_grade_2)
        }
        else
        {
            imageResultado.setImageResource(R.drawable.obesidad_extrema)
            return getResources().getString(R.string.obesity_grade_3)
        }
    }
}